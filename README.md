# Code Quiz

<!-- Title  -->

This is a quiz app, and will cycle through various questions, present them to the user, calculate a score, and post to a list of high scores.

---

## Table of Contents

<!-- Table of Contents -->

- [About The Project](#about_project)
- [Getting Started](#getting_started)
- [Deployment Location](#deployment_location)
- [Challenges](#challenges)
- [Author Credit](#author_credit)
- [Acknowledgments](#acknowledgments)
- [Resources](#resources)
- [Final Note](#final_note)

---

## About The Project <a id="about_project"></a>

<!-- About the Project -->

**User Story**

`AS A coding boot camp student`

**I WANT** to take a timed quiz on JavaScript fundamentals that stores high scores **SO THAT** I can gauge my progress compared to my peers

**Acceptance Criteria**

`GIVEN I am taking a code quiz`

[x] **WHEN** I click the start button
**THEN** a timer starts and I am presented with a question

[x] **WHEN** I answer a question
**THEN** I am presented with another question

[x] **WHEN** I answer a question incorrectly
**THEN** time is subtracted from the clock

[x] **WHEN** all questions are answered or the timer reaches 0
**THEN** the game is over

[x] **WHEN** the game is over
**THEN** I can save my initials and score

### Built With

<!-- Built With -->

- HTML
- CSS
- Javascript

---

## Getting Started <a id="getting_started"></a>

<!-- Getting Started  -->

### Prerequisites & Dependencies

<!-- Prerequisites & Dependencies-->

- None for this application

### Installation

<!-- Installation -->

- This runs as a web page, so there are no installation requirements

#### Configurables

<!-- Configurables -->

- None for this application

### Running The App

<!-- Running -->

**Quiz**

1. To Start click the "Quiz Me" button
   - this will launch the quiz and start a timer (20 seconds per question)
2. For the question provided, select an answer, and regardless of the answer the next question will be presented
3. Repeat until there are no more questions
4. "Quiz Complete" will display when the quiz has either run out of time or completed the quiz.
   - enter in your name or initials
5. Click "Submit Score"

**Highscores**

1. There is an automatic redirect to the Highscores page
   - here you will see a list of the current highscores
2. "Return Home" takes you back to the main quiz page, from which, you can start the quiz over again.
3. "Clear Scores" resets the score list and reloads the page.

---

## Deployment Location <a id="deployment_location"></a>

<!-- Deployment Location -->

Live Link [View App](https://sempercuriosus.github.io/codequiz/)

Main Page
![Main Page Image](assets/readme_example_images/main_page_ex.png)

Quiz Sample
![Question Preview](assets/readme_example_images/question_ex.png)

High Score Page
![High Score Preview](assets/readme_example_images/highscores_ex.png)

---

## Challenges <a id="challenges"></a>

<!-- Challenges -->

1. Showing and Hiding elements dynamically
   - This was new for me, and it was really neat to actually work with. Dynamic web pages and web sites are really neat to use.
2. Creating an onclick event for the buttons
   - This was also new for me. From prior attempts, I can see that I was using the function call eg `startButton.addEventListener("click", startQuiz());` which was calling the function right away instead of waiting for the event trigger, the click in this case.
3. `<script>` was not loading when the attributes were set
   - `rel="script" type="script"`
   - I am still not sure about this one, but this really threw me for a loop trying to figure this out.

---

## Author Credit <a id="author_credit"></a>

<!-- Author Credits -->

Eric Hulse - SemperCuriosus

## Acknowledgments <a id="acknowledgments"></a>

<!-- Acknowledgments -->

- I used Xpert for some debugging, and I also used the code provided, at times, to see how the coder approached the problems I was facing. NOTE: I only did this after I had some time thinking about and attempting each part.

---

## Resources <a id="resources"></a>

<!-- Resources -->

- https://www.rapidtables.com/web/color/RGB_Color.html
  - used this when getting layout colors
- https://mdigi.tools/darken-color/#530d8d
  - found this neat color picker that lets you darken your base color
- https://cssbud.com/css-generator/css-glow-generator/
  - for button hover glow

## Final Note <a id="final_note"></a>

<!-- Final Note -->

Things are starting to make sense and come together more, and it is good to see how to make pages dynamic, and sometimes how little HTML one can get away with by creating it with JS.

---
