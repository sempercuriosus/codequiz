// console.log("Script Loaded.");

let timer = {
  count: 0,
  instance: null,
  label: document.querySelector("#timer"),
  penalty: 15,

  updateCount: function () {
    // [ about ] :
    // console.log("[ updateLabel ] : called");
    this.label.innerHTML = this.count;
  },
  // [ end : updateLabel ]
};

let user = {
  name: "",
  score: 0,
};

let questionCard = {
  title: document.querySelector("#question_title"),
  choices: document.querySelector("#question_choices"),
  result: document.querySelector("#question_result"),
};

let currentQuestion = {
  number: 0,
  title: "",
  choices: "",
  answer: "",
};

let questions = [
  //
  {
    title: "Commonly used data types DO NOT include:",
    choices: ["strings", "booleans", "alerts", "numbers"],
    answer: "alerts",
  },
  {
    title: "The condition in an if / else statement is enclosed within ____.",
    choices: ["quotes", "curly brackets", "parentheses", "square brackets"],
    answer: "parentheses",
  },
  {
    title: "Arrays in JavaScript can be used to store ____.",
    choices: [
      "numbers and strings",
      "other arrays",
      "booleans",
      "all of the above",
    ],
    answer: "all of the above",
  },
  {
    title:
      "String values must be enclosed within ____ when being assigned to variables.",
    choices: ["commas", "curly brackets", "quotes", "parentheses"],
    answer: "quotes",
  },
  {
    title:
      "A very useful tool used during development and debugging for printing content to the debugger is:",
    choices: ["JavaScript", "terminal / bash", "for loops", "console.log"],
    answer: "console.log",
  },
];

// sound effects
var sfxRight = new Audio("/Develop/assets/sfx/correct.wav");
var sfxWrong = new Audio("/Develop/assets/sfx/incorrect.wav");

// #region Query Select Various DOM Elements

let startButton = document.querySelector("#start_button");
let submitButton = document.querySelector("#score_submit");

let startEl = document.querySelector("#start_quiz");
let questionEl = document.querySelector("#question_card");
let questionChoicesEl = document.querySelector("#question_choices");
let gameOverEl = document.querySelector("#end_quiz");
let resultsEl = document.querySelector("#quiz_results");
let usersNameEl = document.querySelector("#name_submit");

// #endregion

// #region Start and Stop Timers

let startTimer = function () {
  // [ about ] : start a timer
  //   console.log("startTimer", " : called");

  timer.count = questions.length * 20;

  //   timer.label.innerHTML = timer.count;
  timer.updateCount();

  timer.instance = setInterval(function () {
    // start the game with questions
    timerInterval();
  }, 1000);
};
// [ end : startTimer ]

let timerInterval = function () {
  // [ about ] : these are the operations that are being called at the timer's frequency
  //   console.log("[ timerInterval ] : called");

  timer.count--;
  //   timer.label.innerHTML = timer.count;
  timer.updateCount();

  if (timer.count < 0) {
    //making sure that the count does not get set to -1
    timer.count = 0;
    timer.updateCount();
    quizOver();
  }
};
// [ end : timerInterval ]

// #endregion

// #region Start and Stop Quiz

// start game
let startQuiz = function () {
  // [ about ] : Starts a Quiz
  //   console.log("[ startQuiz ] : called");
  timer.label.innerHTML = timer.count;
  timer.updateCount();
  // hide the start quiz items
  hideStartEl();
  unhideQuestionEl();
  //get a question
  getQuestion();
  // start timer
  startTimer();
  //
};
// [ end : startQuiz ]

// start quiz
function quizOver() {
  // [ about ] : the timer has expired and the quiz is done
  //   console.log("quizOver  : called");

  clearInterval(timer.instance);

  user.score = timer.count;

  hideQuestionEl();
  unhidegameOverEl();
  // after quiz is over do :
  //   askUsersName();
}
// [ end : quizOver ]

// #endregion

// #region Questions and Related

// get question to ask
let getQuestion = function () {
  // [ about ] : gets a user question
  //   console.log("[ getQuestion ] : called");

  resetQuestionCard();

  let questionIndex = currentQuestion.number;

  currentQuestion.title = questions[questionIndex].title;
  currentQuestion.choices = questions[questionIndex].choices;
  currentQuestion.answer = questions[questionIndex].answer;

  // update the question_card
  setQuestion();
};
// [ end : getQuestion ]

// display Question
let setQuestion = function () {
  // [ about ] : sets the question to display
  //   console.log("[ setQuestion ] : called");

  questionCard.title.innerHTML =
    "Question " + (currentQuestion.number + 1) + ": " + currentQuestion.title;

  // create a new element for display for each choice present in the array
  for (var i = 0; i < currentQuestion.choices.length; i++) {
    let choiceValue = currentQuestion.choices[i];
    var choiceButton = document.createElement("button");
    choiceButton.className = "question_choice";
    choiceButton.value = choiceValue;

    choiceButton.innerHTML = i + 1 + ". " + choiceValue;

    questionCard.choices.appendChild(choiceButton);
  }
};
// [ end : setQuestion ]

let resetQuestionCard = function () {
  // [ about ] : resets the question card after each question
  //   console.log("[ resetQuestionCard ] : called");
  questionCard.title.innerHTML = "";
  questionCard.choices.innerHTML = [];
  //   questionCard.result.innerHTML = "";
};
// [ end : resetQuestionCard ]

// // check if question is correct
let checkAnswer = function (event) {
  // [ about ] : checks if the answer was correct or not
  //   console.log("[ checkAnswer ] : called");
  var answerButton = event.target;
  let userAnswer = answerButton.value;

  if (!answerButton.matches(".question_choice")) {
    console.log(answerButton);
    return;
  }

  if (currentQuestion.answer !== userAnswer) {
    wrongAnswer();
  } else {
    rightAnswer();
  }

  currentQuestion.number++;

  // next question
  //   if (timer.count > 0) {
  // the above did not work becuase the time was still above 0 even though the quiz was done. both of those conditions need to be checked
  if (currentQuestion.number < questions.length && timer.count > 0) {
    getQuestion();
  } else {
    // console.log("no more questions");
    quizOver();
  }
  //   }
};
// [ end : checkAnswer ]

let wrongAnswer = function () {
  // [ about ] : subtracts time form the timer if the answer provided was wrong
  //   console.log("[ wrongAnswer ] : called");

  questionCard.result.innerHTML = "Wrong!";
  sfxWrong.play();
  resultTimeout();

  timer.count = timer.count - timer.penalty;

  if (timer.count < 0) {
    timer.count = 0;
  }

  //   timer.label.innerHTML = timer.count;
  timer.updateCount();
};

let rightAnswer = function () {
  // [ about ] : the answer was correct
  //   console.log("[ rightAnswer ] : called");

  questionCard.result.innerHTML = "Correct!";
  sfxRight.play();
  resultTimeout();
};

let resultTimeout = function () {
  // [ about ] : shows the last answer result for some time
  //   console.log("[ resultTimeout ] : called");

  setTimeout(function () {
    questionCard.result.innerHTML = "";
  }, 1000);
};
// [ end : resultTimeout ]
// [ end : rightAnswer ]
// [ end : wrongAnswer ]

// #endregion

// #region Hide or UnHide Elements on the page

let hideStartEl = function () {
  // [ about ] : hides the start element when the button is clicked.
  //   console.log("[ hideStartEl ] : called");
  startEl.className = "hidden";
};
// [ end : hideStartEl ]

let unhideQuestionEl = function () {
  // [ about ] :
  //   console.log("[ getQuestionEl ] : called");
  questionEl.className = "";
};
// [ end : getQuestionEl ]

let hideQuestionEl = function () {
  // [ about ] : hides the question element after the quiz is over
  //   console.log("[ hideQuestionEl ] : called");
  questionEl.className = "hidden";
};
// [ end : hideQuestionEl ]
let unhidegameOverEl = function () {
  // [ about ] : unhides the gameOver element after the quiz is over
  //   console.log("[ unhideQuizOver ] : called");
  gameOverEl.className = "";
  resultsEl.innerHTML = user.score;
};
// [ end : unhideQuizOver ]

// #endregion

// #region Scores and Updating LocalStorage

let updateHighscores = function () {
  // [ about ] : get user input, get scores, and update them in local storage
  //   console.log("[ updateHighscores ] : called");
  user.name = usersNameEl.value;

  if (user.name !== "") {
    let highscores = "";

    let newUserScore = {
      "name": user.name,
      "userScore": user.score,
    };

    highscores = getScores();
    highscores.push(newUserScore);

    setScores(highscores);

    location.href = "highscores.html";
  }
};
// [ end : updateHighscores ]

// local storage for user score
let getScores = function () {
  // [ about ] : gets the user's score from localStorage
  //   console.log("[ getScores ] : called");

  // get the object from local storage and parse it
  let highscoresLocalStorage = localStorage.highscores;

  if (!highscoresLocalStorage) {
    highscoresLocalStorage = [];
    localStorage.highscores = JSON.stringify(highscoresLocalStorage);
  }

  let highscores = JSON.parse(highscoresLocalStorage);

  //   if (!highscores) {
  //     highscores = 0;
  //   }

  return highscores;
};
// [ end : getScores ]

let setScores = function (highscores) {
  // [ about ] : sets the user's score to local storage
  //   console.log("[ setScores ] : called");
  // take the object and strigify it
  localStorage.highscores = JSON.stringify(highscores);
};
// [ end : setScores ]

// #endregion

//#region Event Listeners

// start button
startButton.addEventListener("click", startQuiz);

// submit
submitButton.addEventListener("click", updateHighscores);

// check answer
questionChoicesEl.addEventListener("click", checkAnswer);
// #endregion
