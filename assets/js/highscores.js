console.log("Script Loaded");

let clearButton = document.querySelector("#clear");
let scoreList = document.querySelector("#score_list");

// #region Highscores

let getHighscores = function () {
  // [ about ] : get the high scores from local storage
  console.log("[ getHighscores ] : called");
  let highscoresLocalStorage = localStorage.highscores;

  if (!highscoresLocalStorage) {
    highscoresLocalStorage = [];
    localStorage.highscores = JSON.stringify(highscoresLocalStorage);
  }

  let scores = JSON.parse(highscoresLocalStorage);

  scores.sort(function (a, b) {
    return b.userScore - a.userScore;
  });
  let a = true;

  renderScores(scores);
};

let renderScores = function (scores) {
  // [ about ] : takes the highscores and displays them to the user
  console.log("[ renderScores ] : called");

  for (let i = 0; i < scores.length; i++) {
    let listItem = document.createElement("li");

    listItem.innerHTML = scores[i].name + " -  " + scores[i].userScore;

    scoreList.appendChild(listItem);
  }
};
// [ end : renderScores ]
// [ end : getHighscores ]

// #endregion

// #region Clear

let clearHighscores = function () {
  // [ about ] :  clears the local storage of high scores
  console.log("[ clearHighscores ] : called");
  localStorage.removeItem("highscores");

  window.location.reload();
};
// [ end : clearHighscores ]

clearButton.addEventListener("click", clearHighscores);

// #endregion

// just call the scores automatically
getHighscores();
